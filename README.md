## The official QCustomPlot repository has moved to:

**https://gitlab.com/ecme2/QCustomPlot**

Please visit [www.qcustomplot.com](https://www.qcustomplot.com) for downloads, tutorials, examples and more.
